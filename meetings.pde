import processing.video.*;
import processing.serial.*;
import ddf.minim.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;
import ddf.minim.signals.*;
import ddf.minim.spi.*;
import ddf.minim.ugens.*;

Capture cam;
int cameraWidth, cameraHeight;
boolean useSerial;
Serial port;
String portName;
Minim minim;
AudioPlayer sound;

final String STATIC_DIR = "static";
final int NUM_IMAGES = 8;
final int NUM_ROWS = 2;
String captureDir;
int countDownEvery;
int countDownFrom;
int switchEvery;
float capturedImageChance;
int captureGrace;
int serialInitialTimeout;
boolean rotateImage = false;

PImage[] imgs;  
int imgPerRow;
int imgWidth, imgHeight;
int lastSwitch;
int lastAllStatic;
boolean allStatic = false;
int allStaticDuration;
int allStaticEvery;
int cropX, cropY, cropW, cropH;
PGraphics exportedImage;

int countDownStart;
int captureCategory;
boolean capturing = false;
PImage captureImage;
boolean forceRedraw = true;
int lastCapture;
int lastCaptureCategory;

boolean configure = false;
boolean debug = false;

color bgColor = color(255);

void settings() {
  if (debug) {
    size(800, 600);
  } else {
    fullScreen();
  }
}

void setup() {
  noCursor();
  background(bgColor);
  textFont(loadFont("Avenir-Book-96.vlw"));
  listCameras();
  loadConfig();
  validateCaptureDir();
  imgs = new PImage[NUM_IMAGES];
  for (int i = 0; i < NUM_IMAGES; i++) {
    imgs[i] = getImageByCategory(i + 1);
  }
  imgPerRow = NUM_IMAGES / NUM_ROWS;
  imgWidth = width / imgPerRow;
  imgHeight = height / NUM_ROWS;
  cropW = imgWidth;
  cropH = imgHeight;

  cam = new Capture(this, cameraWidth, cameraHeight);
  println("using cam " + cam);
  cam.start(); 

  if (useSerial) {
    printArray(Serial.list());
    port = new Serial(this, portName, 115200);
  }
  minim = new Minim(this);
  sound = minim.loadFile("click.mp3");
  lastCapture = -captureGrace;
}

void drawConfigScreen() {
  textSize(12);
  background(0);
  if (rotateImage) {
    pushMatrix();
    rotate(radians(90));
    image(cam,0,-cam.height);
    popMatrix();
  }
  else {
    set(0, 0, cam);
  }
  stroke(255, 255, 0);
  noFill();
  rect(cropX, cropY, cropW, cropH);
  fill(255);
  text("cropX: "+cropX + " cropY: " + cropY, 0, height-115);
}

void drawDebug() {
  pushStyle();
  fill(0);
  rect(0, 0, 128, 120);
  image(cam, 0, 0, 128, 72);
  fill(255);
  textSize(12);
  text(frameRate, 0, 100);
  popStyle();
}

void switchImage() {
  int category = int(random(NUM_IMAGES))+1;
  if (millis() - lastCapture <= captureGrace) {
    while (category == lastCaptureCategory) {
      category = int(random(NUM_IMAGES))+1;
    }
  }
  PImage img = getImageByCategory(category);
  imgs[category-1] = img; 
  //println("switched " + category);
  lastSwitch = millis();
}

void draw() {
  if (configure) {
    drawConfigScreen();
  } else {
    boolean switched = false;
    int now = millis();
    if (allStatic && now-lastAllStatic > allStaticDuration) {
      print("stopping all static");
      allStatic = false;
      lastAllStatic = millis();
    }
    if (!allStatic && now - lastAllStatic > allStaticEvery && !capturing && now-lastCapture>captureGrace) {
      print("starting all static");
      allStatic();
      switched = true;
    }
    else if (now - lastSwitch > switchEvery && !capturing && !allStatic) {
      switchImage();
      switched = true;
    }
    if (capturing && now - countDownStart >= countDownFrom) {
      capturing = false;
      addImage(captureCategory);
    }
    if (useSerial) {
      readButtons();
    }
    if (switched || forceRedraw) {
      background(bgColor);
      int y = 0;
      for (int i = 0; i < NUM_ROWS; i++) {
        int x = 0;
        for (int j = 0; j < imgPerRow; j++) { 
          PImage img = capturing ? captureImage : imgs[i*imgPerRow+j];
          image(img, x, y, imgWidth, imgHeight);
          x += imgWidth;
        }
        y += imgHeight;
      }
    }
    if (debug) {
      drawDebug();
    }
  }
  if (cam.available()) {
    cam.read();
  }
  if (capturing) {
    drawCaptureImage();
  }
  forceRedraw = false;
}

void allStatic() {
  lastAllStatic = millis();
  for (int i = 0; i < NUM_IMAGES; i++) {
    PImage img = getImage(getStaticCategoryPath(i+1));
    imgs[i] = img;
  }
  allStatic = true;
  lastSwitch = millis();
}

void startCapture(int category) {
  allStatic = false;
  println("starting capture " + category);
  countDownStart = millis();
  captureCategory = category;
  captureImage = getImage(getStaticCategoryPath(category));
  forceRedraw = true;
  capturing = true;
}

void drawCaptureImage() {
  int x=0,y=0;
  if (rotateImage) {
    PImage cropped = cam.get(cropY, cam.height-cropX-cropW, cropH, cropW); 
    pushMatrix();
    translate(width/2, height/2);
    rotate(radians(90));
    image(cropped, -cropW/2, -cropH/2);
    popMatrix();
  }
  else {
    int cat = captureCategory-1;
    int row = cat / imgPerRow;
    int col = cat % imgPerRow;
    x = col * cropW;
    y = row * cropH;
    PImage cropped = cam.get(cropX, cropY, cropW, cropH);
    image(cropped, x, y, cropW, cropH);
  }
  int countDown = (countDownFrom/countDownEvery) - (millis()-countDownStart)/countDownEvery;
  if (countDown > 0) {
    fill(255, 0, 0);
    textSize(128);
    text(countDown, x+cropW/2-32, y+cropH/2+32);
  }
}

void addImage(int category) {
  println("adding image to " + category);
  sound.rewind();
  sound.play();
  String fname = getCapturedCategoryPath(category)+"img"+millis()+".png";
  if (rotateImage) {
      PImage cropped = cam.get(cropY, cam.height-cropX-cropW, cropH, cropW); 
      exportedImage = createGraphics(cropped.height, cropped.width);
      exportedImage.beginDraw();
      exportedImage.translate(exportedImage.width, 0);
      exportedImage.rotate(radians(90));
      exportedImage.image(cropped,0,0);
      exportedImage.endDraw();
      exportedImage.save(fname);
  }
  else {
      PImage cropped = cam.get(cropX, cropY, cropW, cropH);
      cropped.save(fname);
  }
  imgs[category-1] = loadImage(fname);
  lastCapture = millis();
  lastCaptureCategory = category;
}

void keyPressed() {
  if (key >= '1' && key <= '8') {
    int category = key - '0';
    startCapture(category);
  } else if (key == 'd') {
    debug = !debug;
  } else if (key == 'c') {
    configure = !configure;
    if (configure) {
      cursor();
    } else {
      noCursor();
    }
  } else if (key == 's') {
    saveConfig();
  } else if (key == CODED) {
    if (configure) {
      switch (keyCode) {
      case UP:
        cropY--;
        break;
      case DOWN:
        cropY++;
        break;
      case RIGHT:
        cropX++;
        break;
      case LEFT:
        cropX--;
        break;
      }
    }
  }
}

void mousePressed() {
  if (configure) {
    cropX = mouseX;
    cropY = mouseY;
  }
}

void readButtons() {
  char val;
  while (port.available() > 0) {  
    val = (char)port.read(); 
    println("received " + val);
    if (millis() < serialInitialTimeout) {
      println("ignoring on startup");
      return; 
    }
    int category = val - '0';
    if (category >= 1 && category <= NUM_IMAGES) { 
      startCapture(category);
    } 
    else {
      println("ignoring");
    }
  }
}
