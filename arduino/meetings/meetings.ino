int buttonPins[] =  {26, 30, 38, 46, 22, 34, 42, 50};
int buttonVals[] =  {0, 0, 0, 0, 0, 0, 0, 0};
unsigned long lastPress[] =   {0, 0, 0, 0, 0, 0, 0, 0};
int numPins = 8;
unsigned long lastCheck = 0;
bool debug = false;


void setup() {
  for (int i = 0; i < numPins; i++) {
    pinMode(buttonPins[i], INPUT);
  }
  Serial.begin(115200);
}

void loop() {
  if (millis() - lastCheck > 100) {
    for (int i = 0; i < numPins; i++) {
      int val = digitalRead(buttonPins[i]);
      if (val != buttonVals[i]) {
        if (val == HIGH) {
          if (millis() - lastPress[i] > 1000) {
            lastPress[i] = millis();
            Serial.print(i+1);
            if (debug) {
              Serial.print(" index:");
              Serial.print(i); 
              Serial.print(" pin:");
              Serial.println(buttonPins[i]); 
                
            }     
          }
        }
        buttonVals[i] = val; 
      }
    }
    lastCheck = millis();
  }
  
}
