### starting the application
meetings shortcut on the desktop

### stopping the application
press esc (you may need to click the mouse on the screen)

### managing images
All images are stored in the images directory on the desktop, grouped by category.
Remove files you do not want to be shown

### updating code
* open git bash from toolbar
* cd ~/Desktop/guyy/sketchbook/meetings
* git pull
* open processing from toolbar
* open recent - sketchbook/meetings
* file/export 




