void validateCaptureDir() {
  File dir = new File(captureDir);
  if (!dir.exists()) {
    println("capture dir does not exist! " + captureDir);
    System.exit(-1); 
  }
  for (int i = 0; i < NUM_IMAGES; i++) {
    int category = i + 1;
    String categoryPath = captureDir + File.separator + category;
    File categoryDir = new File(categoryPath);
    if (!categoryDir.exists()) {
      println("creating directory for category " + category);
      categoryDir.mkdir();
    }
  }
  
}

PImage getImageByCategory(int category) {
  boolean captured = random(1) <= capturedImageChance;
  String categoryPath = captured ? getCapturedCategoryPath(category) : getStaticCategoryPath(category);
  PImage image = getImage(categoryPath);
  if (image == null) { // should only happen in initial run, when there are no captured images
    return getImage(getStaticCategoryPath(category));
  }
  return image;
}

String getStaticCategoryPath(int category) {
  return dataPath(STATIC_DIR) + File.separator + category + File.separator;
}

String getCapturedCategoryPath(int category) {
  return captureDir + File.separator + category + File.separator;
}

PImage getImage(String categoryPath) {
  String[] fileNames = listFileNames(categoryPath);
  if (fileNames == null || fileNames.length == 0) {
    println("no files in " + categoryPath);
    return null;   
  }
  String imageName = fileNames[int(random(fileNames.length))];
  return loadImage(categoryPath+imageName);
}

String[] listFileNames(String dir) {
  java.io.FilenameFilter pngFilter = new java.io.FilenameFilter() {
    public boolean accept(File dir, String name) {
      return name.toLowerCase().endsWith(".png");
    }
  };
  File file = new File(dir);
  if (file.isDirectory()) {
    String names[] = file.list(pngFilter);
    return names;
  } else {
    // If it's not a directory
    return null;
  }
}

void listCameras() {
  String[] cameras = Capture.list();
  if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } else {
    println("Available cameras:");
    for (int i = 0; i < cameras.length; i++) {
      println(i + " " + cameras[i]);
    }
  } 
}

void loadConfig() {
  JSONObject json = loadJSONObject("config.json");
  portName = json.getString("port");
  cameraWidth = json.getInt("cameraWidth");
  cameraHeight = json.getInt("cameraHeight");
  cropX = json.getInt("cropX");
  cropY = json.getInt("cropY");
  countDownEvery = json.getInt("countDownEvery");
  countDownFrom = json.getInt("countDownFrom");
  switchEvery = json.getInt("switchEvery");
  captureGrace = json.getInt("captureGrace");
  capturedImageChance = json.getFloat("capturedImageChance");
  useSerial = json.getBoolean("useSerial");
  captureDir = json.getString("captureDir");
  serialInitialTimeout = json.getInt("serialInitialTimeout");
  allStaticEvery = json.getInt("allStaticEvery");
  allStaticDuration = json.getInt("allStaticDuration");
  rotateImage = json.getBoolean("rotateImage");
}

void saveConfig() {  
  JSONObject json = new JSONObject();
  json.setString("port", portName);
  json.setInt("cameraWidth", cameraWidth);
  json.setInt("cameraHeight", cameraHeight);
  json.setInt("cropX", cropX);
  json.setInt("cropY", cropY);
  json.setInt("countDownEvery", countDownEvery);
  json.setInt("countDownFrom", countDownFrom);
  json.setInt("switchEvery", switchEvery);
  json.setInt("captureGrace", captureGrace);
  json.setInt("serialInitialTimeout", serialInitialTimeout);
  json.setFloat("capturedImageChance", capturedImageChance);
  json.setBoolean("useSerial", useSerial);
  json.setString("captureDir", captureDir);
  json.setBoolean("rotateImage", rotateImage);
  json.setInt("allStaticEvery", allStaticEvery);
  json.setInt("allStaticDuration",allStaticDuration);
  saveJSONObject(json, "data/config.json");
}
